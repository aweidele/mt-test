import "./App.scss";
import React from "react";
import SolutionOption from "./components/solution-option.jsx";
import Tablist from "./components/tablist.jsx";
import Tab from "./components/tab.jsx";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUndo } from "@fortawesome/free-solid-svg-icons";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import { faChevronUp } from "@fortawesome/free-solid-svg-icons";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      dropdownOpen: false,
      activeSolution: -1,
      activeTab: -1,
      solutions: [
        {
          heading: "Product",
          id: 1,
          tabs: [],
        },
        {
          heading: "Industry (Choose this one)",
          id: 2,
          tabs: [
            {
              name: "Manufacturing",
              id: 1,
            },
            {
              name: "Consumer Goods",
              id: 2,
            },
            {
              name: "Government",
              id: 3,
            },
            {
              name: "Healthcare",
              id: 4,
            },
            {
              name: "Energy & Resources",
              id: 5,
            },
            {
              name: "Academics",
              id: 6,
            },
          ],
        },
        {
          heading: "Department",
          id: 3,
          tabs: [],
        },
        {
          heading: "Analytics Needs",
          id: 4,
          tabs: [],
        },
      ],
      tabs: [
        {
          left: (
            <React.Fragment>
              <h2>Minitab and Healthcare</h2>
              <ul>
                <li>
                  Sed mollis, eros et ultrices tempus, mauris ipsum aliquam
                  libero, non adipiscing dolor urna a orci. In dui magna,
                  posuere eget, vestibulum et, tempor auctor, justo.
                </li>
                <li>
                  Phasellus leo dolor, tempus non, auctor et, hendrerit quis,
                  nisi. Curabitur nisi.
                </li>
                <li>
                  Proin pretium, leo ac pellentesque mollis, felis nunc ultrices
                  eros, sed gravida augue augue mollis justo. Nullam cursus
                  lacinia erat.
                </li>
              </ul>
            </React.Fragment>
          ),
          articles: [
            {
              title: "Case Study",
              description:
                "Title Text Style Ut aliquip ex ea commodo consequat, duis aute irure dolor in reprehenderit.",
            },
            {
              title: "Blog Post",
              description:
                "Title Text Style Ut aliquip ex ea commodo consequat, duis",
            },
            {
              title: "Lorem Ipsum",
              description:
                "Title Text Style Ut aliquip ex ea commodo consequat, duis",
            },
            {
              title: "Webinar",
              description:
                "Title Text Style Ut aliquip ex ea commodo consequat, duis",
            },
          ],
        },
      ],
      defaultTab: (
        <React.Fragment>
          <h2>Minitab is here to help you solve your business problems.</h2>
          <p>
            Subhead Copy Lorem ipsum dolor sit amet, consectetur adipiscing
            elit, sed do eiusmod
          </p>
        </React.Fragment>
      ),
    };
  }

  handleDropdownSelect = (k) => {
    this.setState({ dropdownOpen: false, activeSolution: k, activeTab: -1 });
  };

  displayTabs = () => {
    const activeSolution = this.state.activeSolution;
    if (this.state.activeSolution === -1) {
      return this.state.defaultTab;
    } else {
      const thisSolution = this.state.solutions[activeSolution];
      return <Tablist solution={thisSolution} onTab={this.handleTab} />;
    }
  };

  buttonState = () => {
    const activeSolution = this.state.activeSolution;
    if (activeSolution === -1) {
      return "Choose One";
    }
    return this.state.solutions[activeSolution].heading;
  };

  // Tab display
  displayTab = () => {
    if (this.state.activeTab === -1) return;
    return <Tab content={this.state.tabs[0]} />;
  };

  handleTab = () => {
    this.setState({ activeTab: 1 });
  };

  handleReset = () => {
    this.setState({ dropdownOpen: false, activeSolution: -1, activeTab: -1 });
  };

  // Dropdown open?
  handleDropdownOpen = () => {
    this.setState({ dropdownOpen: this.state.dropdownOpen ? false : true });
  };
  dropdownOpen = () =>
    `tabs__dropdown-wrapper${this.state.dropdownOpen ? " open" : ""}`;
  dropdownOpenChevron = () => {
    if (!this.state.dropdownOpen)
      return <FontAwesomeIcon icon={faChevronDown} />;
    if (this.state.dropdownOpen) return <FontAwesomeIcon icon={faChevronUp} />;
  };

  render() {
    return (
      <div className="tabs">
        <div className="tabs__inner">
          <div className="tabs__selector">
            <div>
              <span>View Solutions by:</span>
              <button onClick={this.handleReset}>
                <FontAwesomeIcon icon={faUndo} /> Reset
              </button>
            </div>
            <div className={this.dropdownOpen()}>
              <button onClick={() => this.handleDropdownOpen()}>
                {this.buttonState()}
                {this.dropdownOpenChevron()}
              </button>
              <ul className="tabs__dropdown">
                {this.state.solutions.map((solution, i) => (
                  <SolutionOption
                    key={i}
                    solution={solution}
                    onSelect={this.handleDropdownSelect}
                    index={i}
                  />
                ))}
              </ul>
            </div>
          </div>

          <div className="tabs__tabslist">{this.displayTabs()}</div>
          {this.displayTab()}
        </div>
      </div>
    );
  }
}

export default App;
