import React, { Component } from "react";
class Tablist extends Component {
  render() {
    return (
      <React.Fragment>
        <div>Choose your {this.props.solution.heading}</div>
        <div className="tabs__tabslist-tabs">
          {this.props.solution.tabs.map((tab, i) => (
            <button key={i} onClick={this.props.onTab}>
              {tab.name}
            </button>
          ))}
        </div>
      </React.Fragment>
    );
  }
}

export default Tablist;
