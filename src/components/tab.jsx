import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";
class Tab extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="tabs__tab">
          <div className="richtext">{this.props.content.left}</div>
          <div className="tabs__articles">
            <div className="tabs__featured">
              <a href="/">
                <figure>
                  <img
                    src="https://via.placeholder.com/288x193.png?text=Thumbnail+Image"
                    alt="Thumbnail"
                  />
                </figure>
                <h2>{this.props.content.articles[0].title}</h2>
                <p>
                  {this.props.content.articles[0].description}{" "}
                  <FontAwesomeIcon icon={faChevronRight} />
                </p>
              </a>
            </div>
            <div className="tabs__articles-list">
              {this.props.content.articles.slice(1).map((article, i) => (
                <div key={i}>
                  <a href="/">
                    <h2>{article.title}</h2>
                    <p>
                      {article.description}{" "}
                      <FontAwesomeIcon icon={faChevronRight} />
                    </p>
                  </a>
                </div>
              ))}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Tab;
