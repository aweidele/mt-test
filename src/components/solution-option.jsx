import React, { Component } from "react";
class SolutionOption extends Component {
  render() {
    return (
      <li>
        <button onClick={() => this.props.onSelect(this.props.index)}>
          {this.props.solution.heading}
        </button>
      </li>
    );
  }
}

export default SolutionOption;
